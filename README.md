
# Project 4 task 5

## Мой ник в школе 21 tangelos@student.21-school.ru

## Мой ник в школе 21 lemmiwig@student.21-school.ru

## Мой ник в школе 21  melwynga@student.21-school.ru


### Что такое git bash?
Git Bash - это приложение, которое обеспечивает работу командной строки Git в операционной системе. Это оболочка командной строки для включения git с помощью командной строки в системе. Оболочка-это терминальное приложение, используемое для взаимодействия с операционной системой с помощью письменных команд. Git Bash включает в себя такие инструменты, как Git, SSH, Bash, OpenSSL, который позволяет работать с репозиториями Git, создавать и редактировать файлы, выполнять команды Git и многое другое.
### Что делает команда git pull? Чем она отличается от git push?
Команда git pull — это автоматизированная версия команды git fetch и git merge. Она загружает ветку из удаленного репозитория и сразу же объединяет ее с текущей веткой. Если изменения не приводят к конфликтам, Git создает новый коммит, содержащий все изменения, и обновляет локальную ветку. Если же изменения приводят к конфликтам, Git сообщает об этом, и вы должны решить конфликты вручную, путем редактирования конфликтных файлов, прежде чем выполнить коммит изменений.
git pull может привести к нежелательным изменениям в локальной ветке, если удаленный репозиторий был обновлен другими пользователями. Поэтому перед выполнением команды git pull рекомендуется убедиться, что ваша локальная ветка находится в том же состоянии, что и удаленная ветка. Для этого вы можете выполнить команду git fetch для извлечения изменений из удаленного репозитория и просмотреть их с помощью команды git log или git diff

Команда git push противоположна команде извлечения (с некоторыми оговорками). С ее помощью можно перенести локальную ветку в другой репозиторий и без труда опубликовать поступивший код.

Команда git pull изменяет локальную ветку, в то время как команда git push изменяет удаленную ветку.

Команда git push может привести к отказу отправки изменений, если удаленный репозиторий уже обновлен более новыми изменениями, которых нет в локальной ветке.
    

### Что такое merge request?
"то запрос на вливание одной ветки в другую. Чаще всего вливается ваша ветка в мастер - основную ветку разработки. В первую очередь merge request нужен при работе в команде для проведения код-ревью. Чтобы коллеги увидели новый код и как минимум его просмотрели и оставили замечания или вопросы. Когда запрос на объединение удовлетворяет всем требованиям, он может быть принят и объединен с основной веткой, что приведет к интеграции новой функциональности или исправлений в приложение.

### Чем между собой отличаются команды git status и git log?
Команда git status показывает текущие состояния файлов в рабочей директории и индексе: какие файлы изменены, но не добавлены в индекс; какие ожидают коммита в индексе, т.е изменения, которые были внесены в файлы в вашем рабочем каталоге по сравнению с последним коммитом в репозитории. Вдобавок к этому выводятся подсказки о том, как изменить состояние файлов.

Команда git log используется для просмотра истории коммитов, начиная с самого свежего и уходя к истокам проекта. По умолчанию, она показывает лишь историю текущей ветки, но может быть настроена на вывод истории других, даже нескольких сразу, веток. Каждый коммит содержит информацию, такую как идентификатор коммита (commit ID), автор коммита, дата и время коммита, а также сообщение коммита, которое обычно описывает, что было изменено в данном коммите.
git status используется для проверки текущего состояния репозитория и отображения изменений, которые были внесены в файлы, а git log используется для просмотра истории коммитов в репозитории.

### Что такое submodule? С помощью какой команды можно добавлять сабмодули в свой репозиторий?
Подмодуль-это репозиторий Git внутри другого репозитория Git. Он позволяет включать один репозиторий в другой в качестве подмодуля, что делает удобным работу с несколькими репозиториями одновременно. При добавлении подмодуля в репозитории Git он сохраняет ссылку на коммит внутри подмодуля, что позволяет другим пользователям склонировать основной репозиторий и все его подмодули одновременно. Если в подмодуле что-то меняется, необходимо явно зафиксировать изменения и зафиксировать обновленную ссылку на коммит в основном репозитории.

Для добавления сабмодуля в свой репозиторий используется команда git submodule add. Эта команда принимает два аргумента: URL-адрес репозитория подмодуля и путь в вашем репозитории, где будет храниться подмодуль.
